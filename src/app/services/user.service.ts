import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { UserModel } from "../models/user.model";

@Injectable({
  providedIn: "root",
})
export class UserService {
  root: string = environment.api;

  constructor(private httpClient: HttpClient) {}

  getAllUsers(): Observable<UserModel[]> {
    return this.httpClient.get(this.root) as Observable<UserModel[]>;
  }
}
