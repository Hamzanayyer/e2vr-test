export class UserModel {
  id: number;
  results: [];
  gender: string;
  name: string;
  location: string;
  email: string;
  phone: string;
}
